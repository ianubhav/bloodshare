package com.thegelato.bloodshare;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

import com.parse.ParseException;


/**
 * Created by Anubhav on 05/09/15.
 */
public class ForgotPassword extends Activity {
    private EditText email;
    private Button submit;
    private Button back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        email = (EditText) findViewById(R.id.forgot_pass_email);
        submit = (Button) findViewById(R.id.btn_forgot_pass);
        back = (Button) findViewById(R.id.btn_back_to_login);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ForgotPassword.this, DispatchActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_right_in,R.anim.push_right_out);
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                boolean validationError = false;
                StringBuilder validationErrorMessage =
                        new StringBuilder("Please ");
                if (isEmpty(email)) {
                    validationError = true;
                    validationErrorMessage.append("Enter your E-mail");
                }
                validationErrorMessage.append(".");

                // If there is a validation error, display the error
                if (validationError) {
                    Toast.makeText(ForgotPassword.this, validationErrorMessage.toString(), Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                final ProgressDialog dlg = new ProgressDialog(ForgotPassword.this);
                dlg.setTitle("Please wait.");
                dlg.setMessage("Requesting for a Password Change");
                dlg.show();
                ParseUser.requestPasswordResetInBackground(email.getText().toString(),
                        new RequestPasswordResetCallback() {
                            public void done(ParseException e) {
                                dlg.dismiss();
                                if (e == null) {
                                    Toast.makeText(ForgotPassword.this, "An email was successfully sent with Reset instructions.", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(ForgotPassword.this, DispatchActivity.class);
                                    startActivity(intent);
                                }
                                else if(e.getCode()==ParseException.EMAIL_NOT_FOUND){
                                    Toast.makeText(ForgotPassword.this, "Email Not Found", Toast.LENGTH_LONG).show();

                                }
                                else if(e.getCode()==ParseException.INVALID_EMAIL_ADDRESS){
                                    Toast.makeText(ForgotPassword.this, "Please Enter a valid Email", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    Log.d("ForgotPassword",e.toString());
                                }
                            }

                        });
            }
        });
    }
    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        }
        else {

            return true;

        }
    }
}