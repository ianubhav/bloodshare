package com.thegelato.bloodshare;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.rey.material.widget.Button;
import com.squareup.picasso.Picasso;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.ExecutionException;

public class LoginActivity extends Activity {

    private EditText userId;
    private EditText userPassword;
    private Button normal_login;
    private TextView signUp;
    private Button facebook_login;
    private TextView forgotPassword;
    private Collection<String> facebook_permission;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        facebook_permission= Arrays.asList("public_profile", "email","user_friends");
        normal_login = (Button) findViewById(R.id.login_act_normal_log);
        signUp = (TextView) findViewById(R.id.log_act_create_id);
        userId = (EditText) findViewById(R.id.login_act_id);
        userPassword = (EditText) findViewById(R.id.login_act_pass);
        facebook_login =(Button) findViewById(R.id.login_act_fb_log);
        forgotPassword = (TextView) findViewById(R.id.log_act_for_pass);

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(LoginActivity.this,ForgotPassword.class);
                startActivity(myIntent);
                overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            }
        });
        facebook_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseFacebookUtils.logInWithReadPermissionsInBackground((Activity) v.getContext(), facebook_permission, facebookLoginCallbackV4);
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(LoginActivity.this,CreateAccount.class);
                startActivity(myIntent);
                overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            }
        });
        normal_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean validationError = false;
                StringBuilder validationErrorMessage =
                        new StringBuilder("Please ");
                if (isEmpty(userId)) {
                    validationError = true;
                    validationErrorMessage.append("Enter your E-mail");
                }
                if (isEmpty(userPassword)) {
                    if (validationError) {
                        validationErrorMessage.append(" and\n");
                    }
                    validationError = true;
                    validationErrorMessage.append("Enter your Password");
                }
                validationErrorMessage.append(".");

                // If there is a validation error, display the error
                if (validationError) {
                    Toast.makeText(LoginActivity.this, validationErrorMessage.toString(), Toast.LENGTH_LONG)
                            .show();
                    return;
                }

                final ProgressDialog dlg = new ProgressDialog(LoginActivity.this);
                dlg.setTitle("Please wait.");
                dlg.setMessage("Logging in.  Please wait.");
                dlg.show();
                ParseUser.logInInBackground(userId.getText().toString(), userPassword.getText()
                        .toString(), new LogInCallback() {

                    @Override
                    public void done(ParseUser user, ParseException e) {
                        dlg.dismiss();
                        if (e != null) {
                            // Show the error message
                            Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        } else {
                            // Start an intent for the dispatch activity
                            Intent intent = new Intent(LoginActivity.this, DispatchActivity.class);
                            startActivity(intent);
                        }
                    }
                });
            }
        });


    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        }
        else {
            return true;
        }
    }

    private LogInCallback facebookLoginCallbackV4 = new LogInCallback() {

        @Override
        public void done(ParseUser user, ParseException e) {
            final ProgressDialog dlg = new ProgressDialog(LoginActivity.this);
            dlg.setTitle("Please wait.");
            dlg.setMessage("Logging In..");
            dlg.show();
            if (user == null) {
                if (e != null) {
                    Log.d("face",e.toString());
                }
            }
            else if (user.isNew()) {
                GraphRequest request = GraphRequest.newMeRequest(
                        AccessToken.getCurrentAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject fbUser, GraphResponse response) {
                                Bitmap bm = null;
                                String url = null;
                                url = "https://graph.facebook.com/"+AccessToken.getCurrentAccessToken().getUserId()+"/picture?type=large&access_token="+AccessToken.getCurrentAccessToken().getToken();
                                class image extends AsyncTask<String, Void, Bitmap> {
                                    @Override
                                    protected Bitmap doInBackground(String... params) {
                                        Bitmap bm = null;
                                        try {
                                            bm = Picasso.with(getBaseContext()).load(params[0]).get();
                                        } catch (IOException e1) {
                                            e1.printStackTrace();
                                        }
                                        return bm;
                                    }
                                }
                                ;
                                try {
                                    bm = new image().execute(url).get();
                                } catch (InterruptedException e1) {
                                    e1.printStackTrace();
                                } catch (ExecutionException e1) {
                                    e1.printStackTrace();
                                }
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bm.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                                byte[] image = stream.toByteArray();
                                ParseFile file = new ParseFile("picture.jpg", image);
                                ParseUser parseUser = ParseUser.getCurrentUser();
                                if(fbUser!=null&&parseUser!=null)
                                {
                                    parseUser.put("name", fbUser.optString("name"));
                                    if(fbUser.has("email")) {
                                        parseUser.put("email", fbUser.optString("email"));
                                        Log.d("face", "yes email");
                                    }
                                    else {
                                        Log.d("face","no email");
                                    }
                                    parseUser.put("displayPic", file);
                                    parseUser.put("gotDetails",false);
                                    parseUser.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            Intent intent = new Intent(LoginActivity.this, DispatchActivity.class);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
                                        }
                                    });
                                }
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,picture");
                request.setParameters(parameters);
                request.executeAsync();

            } else if(user.isAuthenticated()){
                dlg.show();
                Intent intent = new Intent(LoginActivity.this, DispatchActivity.class);
                startActivity(intent);
                dlg.dismiss();
                overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            }
            dlg.dismiss();
        }
    };

}
